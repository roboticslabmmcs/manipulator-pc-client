﻿namespace Manipulator_control
{
    partial class Manipulator_control
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Manipulator_control));
            this.log = new System.Windows.Forms.TextBox();
            this.cbPorts = new System.Windows.Forms.ComboBox();
            this.btnConnect = new System.Windows.Forms.Button();
            this.tbSendLine = new System.Windows.Forms.TextBox();
            this.btnSend = new System.Windows.Forms.Button();
            this.servo1 = new System.Windows.Forms.NumericUpDown();
            this.servo2 = new System.Windows.Forms.NumericUpDown();
            this.servo3 = new System.Windows.Forms.NumericUpDown();
            this.servo4 = new System.Windows.Forms.NumericUpDown();
            this.servo5 = new System.Windows.Forms.NumericUpDown();
            this.servo6 = new System.Windows.Forms.NumericUpDown();
            this.servo1_text = new System.Windows.Forms.Label();
            this.servo2_text = new System.Windows.Forms.Label();
            this.servo3_text = new System.Windows.Forms.Label();
            this.servo4_text = new System.Windows.Forms.Label();
            this.servo5_text = new System.Windows.Forms.Label();
            this.servo6_text = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnStatus = new System.Windows.Forms.Button();
            this.log_txt = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.tbProgram = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.start_btn = new System.Windows.Forms.Button();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.pauseUD = new System.Windows.Forms.NumericUpDown();
            this.btnLoad = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.lbProgram = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btn_to_center = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.servo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.servo2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.servo3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.servo4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.servo5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.servo6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pauseUD)).BeginInit();
            this.SuspendLayout();
            // 
            // log
            // 
            this.log.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.log.Location = new System.Drawing.Point(679, 113);
            this.log.Multiline = true;
            this.log.Name = "log";
            this.log.ReadOnly = true;
            this.log.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.log.Size = new System.Drawing.Size(459, 293);
            this.log.TabIndex = 15;
            // 
            // cbPorts
            // 
            this.cbPorts.FormattingEnabled = true;
            this.cbPorts.Location = new System.Drawing.Point(679, 30);
            this.cbPorts.Name = "cbPorts";
            this.cbPorts.Size = new System.Drawing.Size(153, 21);
            this.cbPorts.TabIndex = 10;
            this.cbPorts.SelectedIndexChanged += new System.EventHandler(this.ports_SelectedIndexChanged);
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(838, 28);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(84, 22);
            this.btnConnect.TabIndex = 11;
            this.btnConnect.Text = "СОЕДИНИТЬ";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.connect_Click);
            // 
            // tbSendLine
            // 
            this.tbSendLine.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSendLine.Location = new System.Drawing.Point(679, 425);
            this.tbSendLine.Name = "tbSendLine";
            this.tbSendLine.Size = new System.Drawing.Size(351, 20);
            this.tbSendLine.TabIndex = 16;
            // 
            // btnSend
            // 
            this.btnSend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSend.Location = new System.Drawing.Point(1049, 412);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(89, 45);
            this.btnSend.TabIndex = 17;
            this.btnSend.Text = "ОТПРАВИТЬ";
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Click += new System.EventHandler(this.send_Click);
            // 
            // servo1
            // 
            this.servo1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.servo1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.servo1.Location = new System.Drawing.Point(11, 30);
            this.servo1.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.servo1.Name = "servo1";
            this.servo1.Size = new System.Drawing.Size(76, 44);
            this.servo1.TabIndex = 1;
            this.servo1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.servo1.Value = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.servo1.ValueChanged += new System.EventHandler(this.servo1_ValueChanged);
            // 
            // servo2
            // 
            this.servo2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.servo2.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.servo2.Location = new System.Drawing.Point(93, 30);
            this.servo2.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.servo2.Name = "servo2";
            this.servo2.Size = new System.Drawing.Size(76, 44);
            this.servo2.TabIndex = 2;
            this.servo2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.servo2.Value = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.servo2.ValueChanged += new System.EventHandler(this.servo2_ValueChanged);
            // 
            // servo3
            // 
            this.servo3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.servo3.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.servo3.Location = new System.Drawing.Point(175, 30);
            this.servo3.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.servo3.Name = "servo3";
            this.servo3.Size = new System.Drawing.Size(77, 44);
            this.servo3.TabIndex = 3;
            this.servo3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.servo3.Value = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.servo3.ValueChanged += new System.EventHandler(this.servo3_ValueChanged);
            // 
            // servo4
            // 
            this.servo4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.servo4.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.servo4.Location = new System.Drawing.Point(258, 30);
            this.servo4.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.servo4.Name = "servo4";
            this.servo4.Size = new System.Drawing.Size(77, 44);
            this.servo4.TabIndex = 4;
            this.servo4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.servo4.Value = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.servo4.ValueChanged += new System.EventHandler(this.servo4_ValueChanged);
            // 
            // servo5
            // 
            this.servo5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.servo5.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.servo5.Location = new System.Drawing.Point(341, 30);
            this.servo5.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.servo5.Name = "servo5";
            this.servo5.Size = new System.Drawing.Size(76, 44);
            this.servo5.TabIndex = 5;
            this.servo5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.servo5.Value = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.servo5.ValueChanged += new System.EventHandler(this.servo5_ValueChanged);
            // 
            // servo6
            // 
            this.servo6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.servo6.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.servo6.Location = new System.Drawing.Point(423, 30);
            this.servo6.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.servo6.Minimum = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.servo6.Name = "servo6";
            this.servo6.Size = new System.Drawing.Size(76, 44);
            this.servo6.TabIndex = 6;
            this.servo6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.servo6.Value = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.servo6.ValueChanged += new System.EventHandler(this.servo6_ValueChanged);
            // 
            // servo1_text
            // 
            this.servo1_text.AutoSize = true;
            this.servo1_text.Location = new System.Drawing.Point(8, 14);
            this.servo1_text.Name = "servo1_text";
            this.servo1_text.Size = new System.Drawing.Size(63, 13);
            this.servo1_text.TabIndex = 19;
            this.servo1_text.Text = "Основание";
            // 
            // servo2_text
            // 
            this.servo2_text.AutoSize = true;
            this.servo2_text.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.servo2_text.Location = new System.Drawing.Point(90, 13);
            this.servo2_text.Name = "servo2_text";
            this.servo2_text.Size = new System.Drawing.Size(65, 13);
            this.servo2_text.TabIndex = 20;
            this.servo2_text.Text = "Подъем гл.";
            // 
            // servo3_text
            // 
            this.servo3_text.AutoSize = true;
            this.servo3_text.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.servo3_text.Location = new System.Drawing.Point(172, 14);
            this.servo3_text.Name = "servo3_text";
            this.servo3_text.Size = new System.Drawing.Size(66, 13);
            this.servo3_text.TabIndex = 21;
            this.servo3_text.Text = "Подъем ср.";
            // 
            // servo4_text
            // 
            this.servo4_text.AutoSize = true;
            this.servo4_text.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.servo4_text.Location = new System.Drawing.Point(255, 13);
            this.servo4_text.Name = "servo4_text";
            this.servo4_text.Size = new System.Drawing.Size(71, 13);
            this.servo4_text.TabIndex = 22;
            this.servo4_text.Text = "Подъем врх.";
            // 
            // servo5_text
            // 
            this.servo5_text.AutoSize = true;
            this.servo5_text.Location = new System.Drawing.Point(348, 14);
            this.servo5_text.Name = "servo5_text";
            this.servo5_text.Size = new System.Drawing.Size(59, 13);
            this.servo5_text.TabIndex = 23;
            this.servo5_text.Text = "Вращение";
            // 
            // servo6_text
            // 
            this.servo6_text.AutoSize = true;
            this.servo6_text.Location = new System.Drawing.Point(436, 13);
            this.servo6_text.Name = "servo6_text";
            this.servo6_text.Size = new System.Drawing.Size(37, 13);
            this.servo6_text.TabIndex = 24;
            this.servo6_text.Text = "Кисть";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(676, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(143, 13);
            this.label1.TabIndex = 25;
            this.label1.Text = "Подключение к COM-порту";
            // 
            // btnStatus
            // 
            this.btnStatus.Location = new System.Drawing.Point(928, 28);
            this.btnStatus.Name = "btnStatus";
            this.btnStatus.Size = new System.Drawing.Size(102, 22);
            this.btnStatus.TabIndex = 12;
            this.btnStatus.Text = "СТАТУС";
            this.btnStatus.UseVisualStyleBackColor = true;
            this.btnStatus.Click += new System.EventHandler(this.status_Click);
            // 
            // log_txt
            // 
            this.log_txt.AutoSize = true;
            this.log_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.log_txt.Location = new System.Drawing.Point(676, 92);
            this.log_txt.Name = "log_txt";
            this.log_txt.Size = new System.Drawing.Size(133, 18);
            this.log_txt.TabIndex = 43;
            this.log_txt.Text = "Принятые данные";
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(587, 30);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(71, 44);
            this.btnAdd.TabIndex = 8;
            this.btnAdd.Text = "Добавить Движение";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.add_btn_Click);
            // 
            // tbProgram
            // 
            this.tbProgram.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.tbProgram.Location = new System.Drawing.Point(14, 113);
            this.tbProgram.Multiline = true;
            this.tbProgram.Name = "tbProgram";
            this.tbProgram.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbProgram.Size = new System.Drawing.Size(644, 293);
            this.tbProgram.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(11, 92);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 18);
            this.label2.TabIndex = 46;
            this.label2.Text = "Программа";
            // 
            // start_btn
            // 
            this.start_btn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.start_btn.BackColor = System.Drawing.Color.PaleTurquoise;
            this.start_btn.Location = new System.Drawing.Point(551, 412);
            this.start_btn.Name = "start_btn";
            this.start_btn.Size = new System.Drawing.Size(107, 45);
            this.start_btn.TabIndex = 14;
            this.start_btn.Text = "ВЫПОЛНИТЬ";
            this.start_btn.UseVisualStyleBackColor = false;
            this.start_btn.Click += new System.EventHandler(this.start_btn_Click);
            // 
            // timer
            // 
            this.timer.Interval = 1000;
            this.timer.Tick += new System.EventHandler(this.run_program);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(514, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 49;
            this.label3.Text = "Пауза";
            // 
            // pauseUD
            // 
            this.pauseUD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.pauseUD.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.pauseUD.Location = new System.Drawing.Point(505, 30);
            this.pauseUD.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.pauseUD.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.pauseUD.Name = "pauseUD";
            this.pauseUD.Size = new System.Drawing.Size(76, 44);
            this.pauseUD.TabIndex = 7;
            this.pauseUD.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.pauseUD.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // btnLoad
            // 
            this.btnLoad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnLoad.Location = new System.Drawing.Point(11, 412);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(107, 45);
            this.btnLoad.TabIndex = 50;
            this.btnLoad.TabStop = false;
            this.btnLoad.Text = "ЗАГРУЗИТЬ";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSave.Location = new System.Drawing.Point(124, 412);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(107, 45);
            this.btnSave.TabIndex = 51;
            this.btnSave.TabStop = false;
            this.btnSave.Text = "СОХРАНИТЬ";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.DefaultExt = "mpt";
            this.openFileDialog.Filter = "Программа манипулятора|*.mpt";
            this.openFileDialog.RestoreDirectory = true;
            this.openFileDialog.Title = "Открытие программы манипулятора";
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.DefaultExt = "mpt";
            this.saveFileDialog.Filter = "Программа манипулятора|*.mpt";
            this.saveFileDialog.RestoreDirectory = true;
            this.saveFileDialog.Title = "Сохранение программы манипулятора";
            // 
            // lbProgram
            // 
            this.lbProgram.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lbProgram.Enabled = false;
            this.lbProgram.FormattingEnabled = true;
            this.lbProgram.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9"});
            this.lbProgram.Location = new System.Drawing.Point(486, 171);
            this.lbProgram.Name = "lbProgram";
            this.lbProgram.ScrollAlwaysVisible = true;
            this.lbProgram.Size = new System.Drawing.Size(279, 147);
            this.lbProgram.TabIndex = 52;
            this.lbProgram.Visible = false;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(255, 426);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(256, 23);
            this.label4.TabIndex = 53;
            this.label4.Text = "Антон Федяшов, МЕХМАТ ЮФУ ";
            // 
            // btn_to_center
            // 
            this.btn_to_center.Location = new System.Drawing.Point(587, 81);
            this.btn_to_center.Name = "btn_to_center";
            this.btn_to_center.Size = new System.Drawing.Size(71, 23);
            this.btn_to_center.TabIndex = 54;
            this.btn_to_center.Text = "В центр";
            this.btn_to_center.UseVisualStyleBackColor = true;
            this.btn_to_center.Click += new System.EventHandler(this.btn_to_center_Click);
            // 
            // Manipulator_control
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1148, 466);
            this.Controls.Add(this.btn_to_center);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lbProgram);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnLoad);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pauseUD);
            this.Controls.Add(this.start_btn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbProgram);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.log_txt);
            this.Controls.Add(this.btnStatus);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.servo6_text);
            this.Controls.Add(this.servo5_text);
            this.Controls.Add(this.servo4_text);
            this.Controls.Add(this.servo3_text);
            this.Controls.Add(this.servo2_text);
            this.Controls.Add(this.servo1_text);
            this.Controls.Add(this.servo6);
            this.Controls.Add(this.servo5);
            this.Controls.Add(this.servo4);
            this.Controls.Add(this.servo3);
            this.Controls.Add(this.servo2);
            this.Controls.Add(this.servo1);
            this.Controls.Add(this.btnSend);
            this.Controls.Add(this.tbSendLine);
            this.Controls.Add(this.btnConnect);
            this.Controls.Add(this.cbPorts);
            this.Controls.Add(this.log);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(1164, 505);
            this.Name = "Manipulator_control";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Управление манипулятором";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.servo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.servo2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.servo3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.servo4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.servo5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.servo6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pauseUD)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox log;
        private System.Windows.Forms.ComboBox cbPorts;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.TextBox tbSendLine;
        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.NumericUpDown servo1;
        private System.Windows.Forms.NumericUpDown servo2;
        private System.Windows.Forms.NumericUpDown servo3;
        private System.Windows.Forms.NumericUpDown servo4;
        private System.Windows.Forms.NumericUpDown servo5;
        private System.Windows.Forms.NumericUpDown servo6;
        private System.Windows.Forms.Label servo1_text;
        private System.Windows.Forms.Label servo2_text;
        private System.Windows.Forms.Label servo3_text;
        private System.Windows.Forms.Label servo4_text;
        private System.Windows.Forms.Label servo5_text;
        private System.Windows.Forms.Label servo6_text;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnStatus;
        private System.Windows.Forms.Label log_txt;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.TextBox tbProgram;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button start_btn;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown pauseUD;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.ListBox lbProgram;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn_to_center;
    }
}

