﻿using System;
using System.Windows.Forms;
using System.IO.Ports;
using System.IO;
using System.Drawing;

namespace Manipulator_control
{
    public partial class Manipulator_control : Form
    {
        private int currentProgramLineIndex = 0;

        public SerialPort port = new SerialPort();
        delegate void SetTextCallback(string text);

        public Manipulator_control()
        {
            InitializeComponent();
        }

        // функиця таймера
        public void run_program(object sender, EventArgs e)
        {
            if (tbProgram.Lines.Length > 0)
            {
                if (currentProgramLineIndex >= tbProgram.Lines.Length)
                    currentProgramLineIndex = 0;

                if (!String.IsNullOrWhiteSpace(tbProgram.Lines[currentProgramLineIndex].Replace("\r", "").Replace("\n", "")))
                {

                    lbProgram.SelectedIndex = currentProgramLineIndex;
                    string command = tbProgram.Lines[currentProgramLineIndex++];
                    RunLine(command);
                }
                else
                    currentProgramLineIndex++;
            }
        }

        public void deparse_string(string command)
        {
            command = command.Substring(1);
            string[] array_of_servos = command.Split(' ');
            servo1.Value = Convert.ToInt32(array_of_servos[0]);
            servo2.Value = Convert.ToInt32(array_of_servos[1]);
            servo3.Value = Convert.ToInt32(array_of_servos[2]);
            servo4.Value = Convert.ToInt32(array_of_servos[3]);
            servo5.Value = Convert.ToInt32(array_of_servos[4]);
            servo6.Value = Convert.ToInt32(array_of_servos[5]);
        }

        private void RunLine(string command)
        {
            int pauseValue = 3;
            string commandValue = String.Empty;
            deparse_string(command);
            int pauseIndex = command.IndexOf("P");
            if (pauseIndex != -1)
            {
                string pauseStringValue = command.Substring(pauseIndex + 1);
                int i = 0;
                if (Int32.TryParse(pauseStringValue, out i))
                    pauseValue = i;
                commandValue = command.Substring(0, pauseIndex - 1);
            }
            else
                commandValue = command;
            timer.Interval = pauseValue * 1000;
            if (port.IsOpen)
                port.WriteLine(commandValue);
        }

        private void SetLogText(string textMessage)
        {
            if (log.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetLogText);
                Invoke(d, new object[] { textMessage });
            }
            else
            {
                log.AppendText(textMessage + "\n");
            }
        }

        #region Установка значений контролов ServoN
        private void SetServo1Value(string angle1)
        {
            if (servo1.InvokeRequired)
            {
                SetTextCallback c = new SetTextCallback(SetServo1Value);
                Invoke(c, new object[] { angle1 });
            }
            else
            {
                servo1.Value = Convert.ToInt32(angle1);
            }
        }

        private void SetServo2Value(string angle2)
        {
            if (servo2.InvokeRequired)
            {
                SetTextCallback c = new SetTextCallback(SetServo2Value);
                Invoke(c, new object[] { angle2 });
            }
            else
            {
                servo2.Value = Convert.ToInt32(angle2);
            }
        }

        private void SetServo3Value(string angle3)
        {
            if (servo3.InvokeRequired)
            {
                SetTextCallback c = new SetTextCallback(SetServo3Value);
                Invoke(c, new object[] { angle3 });
            }
            else
            {
                servo3.Value = Convert.ToInt32(angle3);
            }
        }
        private void SetServo4Value(string angle4)
        {
            if (servo4.InvokeRequired)
            {
                SetTextCallback c = new SetTextCallback(SetServo4Value);
                Invoke(c, new object[] { angle4 });
            }
            else
            {
                servo4.Value = Convert.ToInt32(angle4);
            }
        }
        private void SetServo5Value(string angle5)
        {
            if (servo5.InvokeRequired)
            {
                SetTextCallback c = new SetTextCallback(SetServo5Value);
                Invoke(c, new object[] { angle5 });
            }
            else
            {
                servo5.Value = Convert.ToInt32(angle5);
            }
        }
        private void SetServo6Value(string angle6)
        {
            if (servo6.InvokeRequired)
            {
                SetTextCallback c = new SetTextCallback(SetServo6Value);
                Invoke(c, new object[] { angle6 });
            }
            else
            {
                servo6.Value = Convert.ToInt32(angle6);
            }
        }
        #endregion

        public void OpenConnection()
        {
            if (port.IsOpen)
            {
                port.Close();
                log.AppendText("Closing port, because it was already open!\n");
            }
            else
            {
                port.Open();
                port.ReadTimeout = 1000;
                log.AppendText("Port Opened!\n");
                port.WriteLine("STATUS");
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            foreach (string portname in SerialPort.GetPortNames())
            {
                cbPorts.Items.Add(portname);
            }
            if (cbPorts.Items.Count > 0)
            {
                cbPorts.SelectedIndex = 0;
            }

            port.DataReceived += Port_DataReceived;
        }

        #region Работа с COM портом
        private void Port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                string readstring = port.ReadExisting();
                int number = -1;
                string[] angles = new string[6]; 
                bool result = Int32.TryParse(readstring[0].ToString(), out number);
                SetLogText(readstring);
            }
            catch (TimeoutException)
            {
            }
        }

        private void ports_SelectedIndexChanged(object sender, EventArgs e)
        {
            port.Close();
            port.PortName = cbPorts.SelectedItem.ToString();
            port.BaudRate = 115200;
            port.DataBits = 8;
            port.Parity = Parity.None;
            port.StopBits = StopBits.One;
        }

        private void connect_Click(object sender, EventArgs e)
        {
            OpenConnection();
        }
        #endregion
        #region Отправка значений на сервы
        private void servo1_ValueChanged(object sender, EventArgs e)
        {
            if (port.IsOpen && !timer.Enabled)
                port.WriteLine("A" + servo1.Value.ToString());
        }
        private void servo2_ValueChanged(object sender, EventArgs e)
        {
            if (port.IsOpen && !timer.Enabled)
                port.WriteLine("B" + servo2.Value.ToString());
        }
        private void servo3_ValueChanged(object sender, EventArgs e)
        {
            if (port.IsOpen && !timer.Enabled)
                port.WriteLine("C" + servo3.Value.ToString());
        }
        private void servo4_ValueChanged(object sender, EventArgs e)
        {
            if (port.IsOpen && !timer.Enabled)
                port.WriteLine("D" + servo4.Value.ToString());
        }
        private void servo5_ValueChanged(object sender, EventArgs e)
        {
            if (port.IsOpen && !timer.Enabled)
                port.WriteLine("E" + servo5.Value.ToString());
        }
        private void servo6_ValueChanged(object sender, EventArgs e)
        {
            if (port.IsOpen && !timer.Enabled)
                port.WriteLine("F" + servo6.Value.ToString());
        }
        #endregion

        private void send_Click(object sender, EventArgs e)
        {
            if (port.IsOpen)
                port.WriteLine(tbSendLine.Text);
            tbSendLine.Clear();
        }

        private void status_Click(object sender, EventArgs e)
        {
            if (port.IsOpen)
                port.WriteLine("STATUS");
        }

        private void add_btn_Click(object sender, EventArgs e)
        {
            tbProgram.AppendText("S" + servo1.Text + " " + servo2.Text + " " + servo3.Text + " " + servo4.Text + " " + servo5.Text + " " + servo6.Text + " ");
            tbProgram.AppendText("P" + pauseUD.Text + "\r\n");
        }

        private void start_btn_Click(object sender, EventArgs e)
        {
            currentProgramLineIndex = 0;
            log.Clear();
            if (!timer.Enabled)
            {
                timer.Enabled = true;
                start_btn.Text = "СТОП";
                start_btn.BackColor = Color.Red;
                DisableControls();
            }
            else
            {
                timer.Enabled = false;
                start_btn.Text = "ВЫПОЛНИТЬ";
                start_btn.BackColor = Color.PaleTurquoise;
                EnableControls();
            }
        }

        private void EnableControls()
        {
            servo1.Enabled = true;
            servo2.Enabled = true;
            servo3.Enabled = true;
            servo4.Enabled = true;
            servo5.Enabled = true;
            servo6.Enabled = true;
            btnAdd.Enabled = true;
            btn_to_center.Enabled = true;
            cbPorts.Enabled = true;
            btnConnect.Enabled = true;
            btnStatus.Enabled = true;
            btnSend.Enabled = true;
            tbSendLine.Enabled = true;
            btnSave.Enabled = true;
            btnLoad.Enabled = true;
            tbProgram.Enabled = true;

            tbProgram.Visible = true;
            lbProgram.Visible = false;
        }

        private void DisableControls()
        {
            servo1.Enabled = false;
            servo2.Enabled = false;
            servo3.Enabled = false;
            servo4.Enabled = false;
            servo5.Enabled = false;
            servo6.Enabled = false;
            btnAdd.Enabled = false;
            btn_to_center.Enabled = false;
            cbPorts.Enabled = false;
            btnConnect.Enabled = false;
            btnStatus.Enabled = false;
            btnSend.Enabled = false;
            tbSendLine.Enabled = false;
            btnSave.Enabled = false;
            btnLoad.Enabled = false;
            tbProgram.Enabled = false;

            lbProgram.Items.Clear();

            for (int i = 0; i < tbProgram.Lines.Length; i++)
                lbProgram.Items.Add(tbProgram.Lines[i]);
            lbProgram.Left = tbProgram.Left;
            lbProgram.Top = tbProgram.Top;
            lbProgram.Width = tbProgram.Width;
            lbProgram.Height = tbProgram.Height;
            lbProgram.Visible = true;
            tbProgram.Visible = false;
            
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                tbProgram.Text = File.ReadAllText(openFileDialog.FileName);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                File.WriteAllText(saveFileDialog.FileName, tbProgram.Text);
            }
        }

        private void btn_to_center_Click(object sender, EventArgs e)
        {
            deparse_string("S90 90 90 90 90 90 ");
            if (port.IsOpen)
                port.WriteLine("S90 90 90 90 90 90");
        }
    }
}
